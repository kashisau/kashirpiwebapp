/*
 * This magical function takes an array of floating point numbers and builds a
 * stochastic vector by the most honest means at our disposal.
 * There is an optional argument arbitraryTotal that may be used if the vector
 * only needs to be stochastically-equivilent (i.e. the sum total is equal to a
 * scalar multiple of 1).
 * @param float[] inputVector   An array of integer or floating point values
 *                              that each contribute to a total.
 */
function stochasticEquivalent(inputVector, arbitraryTotal) {
    var vectorSum = 0,
        almostStochastic = [],
        i;
    
    i = inputVector.length;
    while(i--)
        vectorSum += inputVector[i];
    
    i = inputVector.length;
    while(i--)
        almostStochastic.push(inputVector[i] / vectorSum);
    
    return makeStochastic(almostStochastic);    
}

/**
 * Takes a vector whose components should add up to 1 and analyses each
 * component to perform the appropriate rounding to ensure that the sum total
 * of the vector is exactly equal to 1.
 * @param float[] inputVector   An array of floating point values K, where each
 *                              element k in K is between 0 and 1 (inclusively)
 *                              and sum of all k is roughly equal to 1.
 * @return float[]  An array of floating point values K, where each element K
 *                  in k is between 0 and 1 (inclusively) and the sum of all k
 *                  is exactly equal to 1.
 */
function makeStochastic(inputVector) {
    var largestDecimalIndex = null,
        lastLargestDecimalIndex = null,
        stochasticVector = [],
        decParts = [],
        vectorSum = 0,
        targetSum = 1;

    for (var i in inputVector) {
        var intPart = parseInt(inputVector[i] * 100) / 100,
            decPart = (inputVector[i] * 100) % 1;
        
        stochasticVector.push(intPart);
        decParts.push(decPart);
        vectorSum += intPart;
    }
    
    while (vectorSum < targetSum) {
        largestDecimalIndex = null;
        var j = decParts.length;
        while (j--) {
            if (largestDecimalIndex == null) {
                largestDecimalIndex = j;
                continue;
            } else if (decParts[j] >= decParts[lastLargestDecimalIndex]) {
                continue;
            } else if (decParts[j] < decParts[largestDecimalIndex]) {
                continue;
            }
            
            largestDecimalIndex = j;
        }
        lastLargestDecimalIndex = largestDecimalIndex;
        stochasticVector[largestDecimalIndex] += 0.01;
        vectorSum += 0.01;
    }
    
    return stochasticVector;
}