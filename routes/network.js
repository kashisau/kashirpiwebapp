var express = require('express');
var router = express.Router();
var localLAN = '192.168.137.x';

/* GET network ping result. */
router.get(
    '/ping/:hostname',
    function(req, res) {
        var sys = require('sys'),
            exec = require('child_process').exec,
            tail = req.params.hostname.toString();
        
        if ( ! tail.match(/^[0-9]{1,3}$/)) {
            res.json({ target: tail, error: 'invalidTail' });
            return;
        }
        
        hostname = localLAN.replace(/x/, tail);
        function puts(error, stdout, stderr) {
            var received = stdout.match(/([0-9])\sreceived/);

            if (received !== null)
                received = parseInt(received[1], 10) === 1;
            else
                received = false;
            
            res.json(
                {
                    target: tail,
                    result: received
                }
            );
        }
        exec("ping -c 1 -t 1 " + hostname, puts);
    }
);

module.exports = router;