var express = require('express');
var os = require('os');
var router = express.Router();

/* GET home page. */
router.get(
    '/',
    function(req, res) {
        console.time('profileIndex');
        var utHours = parseInt(os.uptime() / 3600),
            utMinutes = parseInt((os.uptime() % 3600) / 60),
            utSeconds = parseInt((os.uptime() % 3600) % 60),
            uptime = [utHours, utMinutes, utSeconds],
            loadAverages = os.loadavg(),
            memTotal = os.totalmem(),
            memFree = os.freemem(),
            memUsed = memTotal - memFree,
            memoryUsage = {
                total: memTotal,
                free: memFree,
                used: memUsed
            },
            renderTime = console.timeEnd('profileIndex'),
            pingResult;
        
        var sys = require('sys');
        var exec = require('child_process').exec;
        function pingOp(error, stdout, stderr) { pingResult = stdout; }
        exec("ping -c 3 localhost", pingOp);
        
        res.render(
            'index',
            {
                title: 'RPi',
                uptime: uptime,
                loadAverages: loadAverages,
                memoryUsage: memoryUsage,
                cpus: os.cpus(),
                renderTime: renderTime,
                pingResult: pingResult
            }
        );
    }
);

module.exports = router;